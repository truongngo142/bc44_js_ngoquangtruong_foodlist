// import { username, age } from "./controller-v1.js";
// console.log(username, age);
// import sayGoodbye from "./controller-v1.js";
// sayGoodbye();

import { Food } from "../../models/v1/model.js";
import hienThiThongTin, { layThongtinTuForm } from "./controller-v1.js";
function themMon() {
  let data = layThongtinTuForm();
  let { maMon, tenMon, loai, giaMon, khuyenMai, tinhTrang, hinhMon, moTa } =
    data;
  let food = new Food(
    maMon,
    tenMon,
    loai,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa
  );
  hienThiThongTin(food);
}
window.themMon = themMon;
