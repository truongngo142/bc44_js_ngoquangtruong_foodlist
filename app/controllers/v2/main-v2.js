import { Food } from "../../models/v1/model.js";
import { layThongtinTuForm } from "../v1/controller-v1.js";
import { renderFoodList } from "./controller-v2.js";

const BASE_URL = "https://64561d985f9a4f23613b11cb.mockapi.io/food";
let fetchFoodList = () => {
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      let foodArr = res.data.map((item) => {
        //destructuring
        let { name, type, discount, img, desc, price, status, id } = item;
        //new object
        let food = new Food(id, name, type, price, discount, status, img, desc);
        //return object
        return food;
      });
      renderFoodList(foodArr.reverse());
    })
    .catch((err) => {
      console.log(err);
    });
};
fetchFoodList();
//xoá
let xoaMon = (id) => {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      fetchFoodList();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.xoaMon = xoaMon;
//thêm món
window.themMon = () => {
  let data = layThongtinTuForm();
  let newfood = {
    name: data.tenMon,
    type: data.loai,
    discount: data.khuyenMai,
    img: data.hinhMon,
    desc: data.moTa,
    price: data.giaMon,
    status: data.tinhTrang,
  };
  axios({
    url: BASE_URL,
    method: "POST",
    data: newfood,
  })
    .then((res) => {
      $("#exampleModal").modal("hide");
      fetchFoodList();
    })
    .catch((err) => {
      console.log(err);
    });
};
// edit mon
window.suaMon = (id) => {
  // lay data mon an
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then((res) => {
      $("#exampleModal").modal("show");
      let { id, type, name, price, img, status, discount, desc } = res.data;
      document.getElementById("foodID").value = id;
      document.getElementById("tenMon").value = name;
      document.getElementById("loai").value = type ? "loai1" : "loai2";
      document.getElementById("giaMon").value = price;
      document.getElementById("khuyenMai").value = discount;
      document.getElementById("tinhTrang").value = status ? "1" : "0";
      document.getElementById("hinhMon").value = img;
      document.getElementById("invalidMoTa").value = desc;
    })
    .catch((err) => {
      console.log(err);
    });
};
// update mon
window.updateMon = () => {
  let item = layThongtinTuForm();
  let newfood = {
    id: item.maMon,
    name: item.tenMon,
    type: item.loai,
    discount: item.khuyenMai,
    img: item.hinhMon,
    desc: item.moTa,
    price: item.giaMon,
    status: item.tinhTrang,
  };
  axios({
    url: `${BASE_URL}/${newfood.id}`,
    method: "PUT",
    data: newfood,
  })
    .then((res) => {
      $("#exampleModal").modal("hide");
      fetchFoodList();
    })
    .catch((err) => {
      console.log(err);
    });
};
