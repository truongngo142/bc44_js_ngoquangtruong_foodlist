// map

//forEach
export let renderFoodList = (foodArr) => {
  let contentHtml = "";
  foodArr.forEach((item) => {
    let { maMon, tenMon, loai, giaMon, khuyenMai, tinhGiaKm, tinhTrang } = item;
    let content = `<tr>
    <td>${maMon}</td>
    <td>${tenMon}</td>
    <td>${loai ? "Chay" : "Mặn"}</td>
    <td>${giaMon}</td>
    <td>${khuyenMai}</td>
    <td>${item.tinhGiaKm()}</td>
    <td>${tinhTrang ? "còn" : "hết"}</td>
    <td>
    <button class ="btn btn-warning" onclick="suaMon(${maMon})">Edit</button>
    <button class ="btn btn-danger" onclick="xoaMon(${maMon})">Delete</button>
    </td>
    </tr>`;
    contentHtml += content;
  });
  document.getElementById("tbodyFood").innerHTML = contentHtml;
};
