export class Food {
  constructor(
    maMon,
    tenMon,
    loai,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa
  ) {
    this.maMon = maMon;
    this.tenMon = tenMon;
    this.loai = loai;
    this.giaMon = giaMon;
    this.khuyenMai = khuyenMai;
    this.tinhTrang = tinhTrang;
    this.hinhMon = hinhMon;
    this.moTa = moTa;
  }
  tinhGiaKm() {
    return this.khuyenMai * (1 - this.giaMon);
  }
}
